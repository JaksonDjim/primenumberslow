/**
 * Created by Zheka on 13.02.2016.
 */
public class PrimeNumber {

    public void loopNumber(long N) {
        for (int i = 0; i < N; i++) {
            boolean prime = isPrime(i);
            if (prime) {
                System.out.print(i + " ");
            }
        }
    }

    private boolean isPrime(int num) {
        if (num < 2) return false;
        if (num == 2) return true;

        for (int i = 2; i < num; i++) {
            if (num % i == 0)
                return false;
        }
        return true;
    }
}